def calculate_square(num):
    """
    This function calculates the square of a number.
    """
    return num * num

def main():
    """
    The main function that demonstrates the usage of calculate_square.
    """
    num = 5
    result = calculate_square(num)
    print(f"The square of {num} is {result}")

if __name__ == "__main__":
    main()
